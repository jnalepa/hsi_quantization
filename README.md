# Towards resource-frugal deep convolutional neural networks for hyperspectral image segmentation
## Jakub Nalepa, Marek Antoniak, Michal Myller, Pablo Ribalta Lorenzo, and Michal Marcinkiewicz

This repository provides supplementary material to the paper listed above (submitted to the Microprocessors and Microsystems journal). It includes: 
- The code for performing quantization-aware training of convolutional neural networks for hyperspectral image segmentation (/code).
- The detailed results obtained for the Salinas Valley and Pavia University datasets (/results). The excel files include the results for each (out of 30) execution.