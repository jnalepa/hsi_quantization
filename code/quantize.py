import os
import time
import math

import csv
import pickle

import tensorflow as tf
import numpy as np

from tensorflow.python.platform import gfile

from hyperspectral_dataset import HyperspectralDataset
from subset import BalancedSubset, ImbalancedSubset


def model_fn(input_layer, input_shape, filters, kernel_size, classes_count):
  """
  Neural network model function
  :param input_layer: layer with input placeholders
  :param input_shape: shape of input data
  :param filters: amount of filters in convolution layer
  :param kernel_size: size of kernel in convolution layer
  :param classes_count: amount of classes in ground truth data
  :return: output layer
  """
  conv2d_layer = tf.keras.layers.Conv2D(filters, (kernel_size, 1), input_shape=input_shape, padding='valid', activation='relu')(input_layer)
  max_pooling_layer = tf.keras.layers.MaxPooling2D(pool_size=(2, 1))(conv2d_layer)
  flatten_layer = tf.keras.layers.Flatten()(max_pooling_layer)
  dense1_layer = tf.keras.layers.Dense(units=512, activation='relu')(flatten_layer)
  dense2_layer = tf.keras.layers.Dense(units=128, activation='relu')(dense1_layer)
  output_layer = tf.keras.layers.Dense(units=classes_count, activation='softmax', name='output_node')(dense2_layer)

  return output_layer


class Quantizer:
  """
  8-bit quantizer
  """

  def __init__(self, artifacts_path: str, model_name: str, filters: int, kernel_size: int):
    """
    Initialize quantizer object
    :param artifacts_path: path to folder where to store artifacts
    :param model_name: name of model files
    :param filters: amount of filters in convolution layer
    :param kernel_size: size of kernel in convolution layer
    """
    os.makedirs(artifacts_path, exist_ok=True)
    self.artifacts_path = artifacts_path
    self.model_name = model_name
    self.filters = filters
    self.kernel_size = kernel_size
    self.training_set = None
    self.validation_set = None
    self.test_set = None
    self.input_shape = None
    self.labels_size = None


  def prepare_sets(self, dataset_path: str, ground_truth_path: str, training_samples_amount: int, validation_samples_amount: int, make_balanced: bool):
    """
    Split dataset into training, validation and test sets
    :param dataset_path: path to dataset
    :param ground_truth_path: path to ground truth
    :param training_samples_amount: how many training samples to extract from dataset
    :param validation_samples_amount: how many validation samples to extract from dataset
    :param make_balanced: whether sets should be balanced
    :return: tuple with training_set, validation_set and test_set
    """
    test_set = HyperspectralDataset(dataset_path, ground_truth_path, neighborhood_size=1)
    test_set.normalize_labels()
    test_set.expand_dims(axis=-1)
    test_set.expand_dims(axis=-1)

    if make_balanced:
      training_set = BalancedSubset(test_set, training_samples_amount + validation_samples_amount)
      validation_set = BalancedSubset(training_set, validation_samples_amount)
    else:
      training_set = ImbalancedSubset(test_set, training_samples_amount + validation_samples_amount)
      validation_set = ImbalancedSubset(training_set, validation_samples_amount)

    max_ = max(training_set.max, validation_set.max)
    min_ = min(training_set.min, validation_set.min)
    training_set.normalize_min_max(min_=min_, max_=max_)
    validation_set.normalize_min_max(min_=min_, max_=max_)
    test_set.normalize_min_max(min_=min_, max_=max_)

    pickle.dump((max_, min_, training_set, validation_set, test_set), open(os.path.join(self.artifacts_path, 'dataset.pickle'), 'wb'))

    self.training_set = training_set
    self.validation_set = validation_set
    self.test_set = test_set
    self.input_shape = training_set.shape[1:]
    self.labels_size = len(np.unique(self.training_set.get_labels()))

    return (training_set, validation_set, test_set)


  def load_sets(self):
    """
    Load previously prepeared datasets into training, validation and test sets
    :return: tuple with training_set, validation_set and test_set
    """
    max_, min_, training_set, validation_set, test_set = pickle.load(open(os.path.join(self.artifacts_path, 'dataset.pickle'), 'rb'))

    self.training_set = training_set
    self.validation_set = validation_set
    self.test_set = test_set
    self.input_shape = training_set.shape[1:]
    self.labels_size = len(np.unique(self.training_set.get_labels()))

    return (training_set, validation_set, test_set)


  def train_float(self, epochs_to_run: int, batch_size: int, go_on: bool = False):
    """
    Train model with float weights
    :param epochs_to_run: how many epochs will be evaluated
    :param batch_size: size of training batch
    :param go_on: whether to load previously trained model
    """
    if self.training_set is None or self.validation_set is None or self.test_set is None:
      raise RuntimeError('You have to run one of: prepare_sets() or load_sets() first')

    tf.reset_default_graph()

    training_data = self.training_set.get_data().astype(np.float32)
    training_dataset = tf.data.Dataset.from_tensor_slices((training_data, self.training_set.get_one_hot_labels(self.labels_size).astype(np.float32))).shuffle(training_data.shape[0], reshuffle_each_iteration=True).batch(batch_size)
    validation_dataset = tf.data.Dataset.from_tensor_slices((self.validation_set.get_data().astype(np.float32), self.validation_set.get_one_hot_labels(self.labels_size).astype(np.float32))).batch(batch_size)
    test_dataset = tf.data.Dataset.from_tensor_slices((self.test_set.get_data().astype(np.float32), self.test_set.get_one_hot_labels(self.labels_size).astype(np.float32))).batch(batch_size)
    iterator = tf.data.Iterator.from_structure(training_dataset.output_types, training_dataset.output_shapes)
    next_x, next_y = iterator.get_next()

    training_init_op = iterator.make_initializer(training_dataset)
    validation_init_op = iterator.make_initializer(validation_dataset)
    test_init_op = iterator.make_initializer(test_dataset)

    logits = model_fn(next_x, self.input_shape, self.filters, self.kernel_size, self.labels_size)

    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=next_y, logits=logits))
    training_step = tf.train.AdamOptimizer(learning_rate=0.001, epsilon=tf.keras.backend.epsilon()).minimize(loss)

    correct_prediction = tf.equal(tf.argmax(logits, 1), tf.argmax(next_y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    sess = tf.InteractiveSession()
    sess.run(tf.global_variables_initializer())

    last_saver = tf.train.Saver()
    best_saver = tf.train.Saver(max_to_keep=1)

    if go_on:
      csv_file = open(os.path.join(self.artifacts_path, '{}-float.csv'.format(self.model_name)), mode='a')
      try:
        lastest_checkpoint = tf.train.latest_checkpoint(self.artifacts_path, latest_filename='{}-float.latest'.format(self.model_name))
        last_saver.restore(sess, lastest_checkpoint)
        start_best_accuracy, start_best_loss, start_best_accuracy_epoch = pickle.load(open(os.path.join(self.artifacts_path, '{}-best-float.pickle'.format(self.model_name)), 'rb'))
      except ValueError as chuj:
        go_on = False
    if not go_on:
      csv_file = open(os.path.join(self.artifacts_path, '{}-float.csv'.format(self.model_name)), mode='w')
      start_best_accuracy = 0
      start_best_loss = float('inf')
      start_best_accuracy_epoch = 0

    csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    best_accuracy, best_accuracy_epoch = self._training_loop('float', epochs_to_run, sess, training_init_op, validation_init_op, test_init_op, training_step, accuracy, loss, last_saver, best_saver, start_best_accuracy, start_best_loss, start_best_accuracy_epoch, csv_writer)

    csv_file.close()

    sess.close()
    print('Float best accuracy {:.15f} %, epoch {}'.format(best_accuracy * 100, best_accuracy_epoch))


  def train_fake_8bit(self, epochs_to_run: int, batch_size: int, go_on: bool = False):
    """
    Train model with fake 8-bit weights
    :param epochs_to_run: how many epochs will be evaluated
    :param batch_size: size of training batch
    :param go_on: whether to load previously trained model
    """
    if self.training_set is None or self.validation_set is None or self.test_set is None:
      raise RuntimeError('You have to run one of: prepare_sets() or load_sets() first')

    tf.reset_default_graph()

    training_data = self.training_set.get_data().astype(np.float32)
    training_dataset = tf.data.Dataset.from_tensor_slices((training_data, self.training_set.get_one_hot_labels(self.labels_size).astype(np.float32))).shuffle(training_data.shape[0]).batch(batch_size)
    validation_dataset = tf.data.Dataset.from_tensor_slices((self.validation_set.get_data().astype(np.float32), self.validation_set.get_one_hot_labels(self.labels_size).astype(np.float32))).batch(batch_size)
    test_dataset = tf.data.Dataset.from_tensor_slices((self.test_set.get_data().astype(np.float32), self.test_set.get_one_hot_labels(self.labels_size).astype(np.float32))).batch(batch_size)
    iterator = tf.data.Iterator.from_structure(training_dataset.output_types, training_dataset.output_shapes)
    next_x, next_y = iterator.get_next()

    training_init_op = iterator.make_initializer(training_dataset)
    validation_init_op = iterator.make_initializer(validation_dataset)
    test_init_op = iterator.make_initializer(test_dataset)

    logits = model_fn(next_x, self.input_shape, self.filters, self.kernel_size, self.labels_size)

    pre_trained_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)

    tf.contrib.quantize.create_training_graph(
      input_graph=tf.get_default_graph(),
      quant_delay=0
    )

    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=next_y, logits=logits))
    training_step = tf.train.AdamOptimizer(learning_rate=0.001, epsilon=tf.keras.backend.epsilon()).minimize(loss=loss, global_step=tf.train.get_global_step())

    correct_prediction = tf.equal(tf.argmax(logits, 1), tf.argmax(next_y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    sess = tf.InteractiveSession()
    sess.run(tf.global_variables_initializer())

    last_saver = tf.train.Saver()
    best_saver = tf.train.Saver(max_to_keep=1)

    is_going_on = False
    if go_on:
      csv_file = open(os.path.join(self.artifacts_path, '{}-8bit.csv'.format(self.model_name)), mode='a')
      try:
        lastest_checkpoint = tf.train.latest_checkpoint(self.artifacts_path, latest_filename='{}-8bit.latest'.format(self.model_name))
        last_saver.restore(sess, lastest_checkpoint)
        start_best_accuracy, start_best_loss, start_best_accuracy_epoch = pickle.load(open(os.path.join(self.artifacts_path, '{}-best-8bit.pickle'.format(self.model_name)), 'rb'))
        is_going_on = True
      except ValueError:
        pass
    if not is_going_on:
      csv_file = open(os.path.join(self.artifacts_path, '{}-8bit.csv'.format(self.model_name)), mode='w')
      restore_saver = tf.train.Saver(var_list=pre_trained_vars)
      start_best_accuracy = 0
      start_best_loss = float('inf')
      start_best_accuracy_epoch = 0
      try:
        restore_saver.restore(sess, os.path.join(self.artifacts_path, '{}-best-float.ckpt'.format(self.model_name)))
      except ValueError:
        raise RuntimeError('You have to run train_float() first')

    csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    best_accuracy, best_accuracy_epoch = self._training_loop('8bit', epochs_to_run, sess, training_init_op, validation_init_op, test_init_op, training_step, accuracy, loss, last_saver, best_saver, start_best_accuracy, start_best_loss, start_best_accuracy_epoch, csv_writer)

    csv_file.close()

    sess.close()
    print('Fake 8-bit best accuracy {:.15f} %, epoch {}'.format(best_accuracy * 100, best_accuracy_epoch))


  def _training_loop(self, postfix: str, epochs_to_run: int, sess: tf.Session, training_init_op, validation_init_op, test_init_op, training_step: tf.Operation, accuracy: tf.Tensor, loss: tf.Tensor, last_saver: tf.train.Saver, best_saver: tf.train.Saver, start_best_accuracy: float, start_best_loss: float, start_best_accuracy_epoch: int, csv_writer: csv.writer):
    epoch_format_length = len(str(epochs_to_run))

    best_accuracy = start_best_accuracy
    best_loss = start_best_loss
    best_accuracy_epoch = start_best_accuracy_epoch

    for epoch in range(1, epochs_to_run + 1):
      sess.run(training_init_op)

      training_inference_times = []
      training_accuracies = []
      training_losses = []
      while True:
        try:
          start_time = time.time()
          _, training_accuracy, training_loss = sess.run([training_step, accuracy, loss])
          training_accuracies.append(training_accuracy)
          training_losses.append(training_loss)
          training_inference_times.append(time.time() - start_time)
        except tf.errors.OutOfRangeError:
          break

      training_inference_time = sum(training_inference_times)
      training_accuracy = np.mean(training_accuracies)
      training_loss = np.mean(training_losses)

      sess.run(validation_init_op)

      validation_inference_times = []
      validation_accuracies = []
      validation_losses = []
      while True:
        try:
          start_time = time.time()
          validation_accuracy, validation_loss = sess.run([accuracy, loss])
          validation_accuracies.append(validation_accuracy)
          validation_losses.append(validation_loss)
          validation_inference_times.append(time.time() - start_time)
        except tf.errors.OutOfRangeError:
          break

      validation_inference_time = sum(validation_inference_times)
      validation_accuracy = np.mean(validation_accuracies)
      validation_loss = np.mean(validation_losses)

      format_text = 'Epoch {:>{epoch_format_length}}, training accuracy {:19.15f}%, loss {:19.15f}, validation accuracy {:19.15f}%, loss {:19.15f}'
      is_better = validation_accuracy > best_accuracy or (validation_accuracy == best_accuracy and validation_loss < best_loss)
      if is_better:
        sess.run(test_init_op)

        test_inference_times = []
        test_accuracies = []
        test_losses = []
        while True:
          try:
            start_time = time.time()
            test_accuracy, test_loss = sess.run([accuracy, loss])
            test_accuracies.append(test_accuracy)
            test_losses.append(test_loss)
            test_inference_times.append(time.time() - start_time)
          except tf.errors.OutOfRangeError:
            break

        test_inference_time = sum(test_inference_times)
        test_accuracy = np.mean(test_accuracies)
        test_loss = np.mean(test_losses)

        format_text += ', test accuracy {:19.15f}%, loss {:19.15f} <-- New best'.format(test_accuracy * 100, test_loss)
      else:
        test_inference_time = -1
        test_accuracy = -1
        test_loss = -1

      print(format_text.format(epoch, training_accuracy * 100, training_loss, validation_accuracy * 100, validation_loss, epoch_format_length=epoch_format_length))

      csv_writer.writerow([
        training_accuracy * 100,
        training_loss,
        training_inference_time,
        validation_accuracy * 100,
        validation_loss,
        validation_inference_time,
        test_accuracy * 100 if test_accuracy >= 0 else '',
        test_loss if test_accuracy >= 0 else '',
        test_inference_time if test_accuracy >= 0 else ''])

      last_saver.save(sess, os.path.join(self.artifacts_path, '{}-{}.ckpt'.format(self.model_name, postfix)), global_step=epoch, latest_filename='{}-{}.latest'.format(self.model_name, postfix))

      if is_better:
        best_saver.save(sess, os.path.join(self.artifacts_path, '{}-best-{}.ckpt'.format(self.model_name, postfix)))
        best_accuracy = validation_accuracy
        best_loss = validation_loss
        best_accuracy_epoch = epoch
        pickle.dump((best_accuracy, best_loss, best_accuracy_epoch), open(os.path.join(self.artifacts_path, '{}-best-{}.pickle'.format(self.model_name, postfix)), 'wb'))

    return (best_accuracy, best_accuracy_epoch)


  def freeze(self):
    """
    Freeze trained fake 8-bit model
    """
    if self.test_set is None:
      raise RuntimeError('You have to run one of: prepare_sets() or load_sets() first')

    tf.reset_default_graph()

    X = tf.placeholder(tf.float32, (None,) + self.input_shape, name='input_node')
    logits = model_fn(X, self.input_shape, self.filters, self.kernel_size, self.labels_size)

    tf.contrib.quantize.create_eval_graph(
      input_graph=tf.get_default_graph()
    )

    sess = tf.InteractiveSession()

    saver = tf.train.Saver()
    try:
      saver.restore(sess, os.path.join(self.artifacts_path, '{}-best-8bit.ckpt'.format(self.model_name)))
    except ValueError:
      raise RuntimeError('You have to run train_fake_8bit() first')

    output_graph_def = tf.graph_util.convert_variables_to_constants(
      sess,
      tf.get_default_graph().as_graph_def(),
      [logits.op.name]
    )

    with tf.gfile.GFile(os.path.join(self.artifacts_path, '{}-frozen.pb'.format(self.model_name)), 'wb') as f:
      f.write(output_graph_def.SerializeToString())

    sess.close()
    print('{} ops in the final graph.'.format(len(output_graph_def.node)))


  def convert(self):
    """
    Convert model to Tensorflow Lite 8-bit model
    """
    if self.test_set is None:
      raise RuntimeError('You have to run one of: prepare_sets() or load_sets() first')
    if not os.path.isfile(os.path.join(self.artifacts_path, '{}-frozen.pb'.format(self.model_name))):
      raise RuntimeError('You have to run freeze() first')

    tf.reset_default_graph()

    converter = tf.lite.TFLiteConverter.from_frozen_graph(os.path.join(self.artifacts_path, '{}-frozen.pb'.format(self.model_name)), ['input_node'], ['output_node/Softmax'], {'input_node': (None,) + self.input_shape})

    input_arrays = converter.get_input_arrays()

    converter.inference_type = tf.lite.constants.QUANTIZED_UINT8
    converter.inference_input_type = tf.lite.constants.QUANTIZED_UINT8
    converter.allow_custom_ops = False
    converter.quantized_input_stats = { input_arrays[0]: (0, 255) }
    converter.post_training_quantize = True

    tflite_model = converter.convert()
    open(os.path.join(self.artifacts_path, '{}.tflite'.format(self.model_name)), 'wb').write(tflite_model)
