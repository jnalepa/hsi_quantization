import os

import tensorflow as tf
import numpy as np

from collections import defaultdict, OrderedDict

def run_interpreter(artifacts_path, model_name, test_set):
  tf.reset_default_graph()

  interpreter = tf.lite.Interpreter(model_path=os.path.join(artifacts_path, '{}.tflite'.format(model_name)))
  interpreter.allocate_tensors()
  input_details = interpreter.get_input_details()
  output_details = interpreter.get_output_details()

  test_data = np.array(test_set.get_data() * 255, dtype=np.uint8)
  test_labels = test_set.get_labels()

  inferred_classes = []
  for index in range(0, test_data.shape[0]):
    data_point = np.reshape(test_data[index,:,:], (1, test_data.shape[1], test_data.shape[2], 1))
    interpreter.set_tensor(input_details[0]['index'], data_point)
    interpreter.invoke()
    output_data = interpreter.get_tensor(output_details[0]['index'])
    inferred_class = output_data.argmax(axis=1)
    inferred_classes.append(inferred_class[0])

  labels_size = len(np.unique(test_labels))
  class_matches = [0] * labels_size * labels_size

  diffs = 0
  class_diffs = defaultdict(int)
  for index in range(len(inferred_classes)):
    diffs += 1 if test_labels[index] != inferred_classes[index] else 0
    class_diffs[int(test_labels[index])] += 1 if test_labels[index] != inferred_classes[index] else 0
    class_matches[int(test_labels[index]) * labels_size + int(inferred_classes[index])] += 1
  acc = 100.0 * (len(inferred_classes) - diffs) / len(inferred_classes)
  print('ovarall acc {}%'.format(acc))

  ordered_diffs = OrderedDict(sorted(class_diffs.items()))
  for class_key, class_diff in ordered_diffs.items():
    class_count = len([z for z in test_labels if z == class_key])
    class_acc = 100.0 * (class_count - class_diff) / class_count
    print('class #{} acc: {}%'.format(class_key + 1, class_acc))

  print('       ', end='')
  for index_x in range(labels_size):
    print('{:5} '.format('#{}'.format(index_x + 1)), end='')
  print('')
  for index_y in range(labels_size):
    print('{:4} '.format('#{}'.format(index_y + 1)), end='')
    for index_x in range(labels_size):
      print('{:5d} '.format(class_matches[index_y * labels_size + index_x]), end='')
    print('')
