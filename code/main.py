#!/usr/bin/python3

import sys

import argparse
import tensorflow as tf
from enum import Enum

from quantize import Quantizer
from interpreter import run_interpreter


class ContinueFrom(Enum):
  prepare_sets = 'prepare_sets'
  train_float = 'train_float'
  train_fake_8bit = 'train_fake_8bit'
  freeze = 'freeze'
  convert = 'convert'
  verify = 'verify'

  def __str__(self):
    return self.value


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_path', type=str,
                        help='Path to the dataset in .npy format')
    parser.add_argument('--gt_path', type=str,
                        help='Path to the ground truth in .npy format')
    parser.add_argument('--artifacts_path', type=str, default='artifacts',
                        help='Path to the output directory in which artifacts will be stored')
    parser.add_argument('--model_name', type=str, default='model01',
                        help='Name of the model in which data will be stored')
    parser.add_argument('--val_samples', type=int, default=25,
                        help='Number of validation samples per class to use')
    parser.add_argument('--balanced', type=bool, default=True,
                        help='Whether each class should have an equal '
                             'number of samples. If True, parameter '
                             'train_samples should be equal to a number of '
                             'samples for each class, if False, paramter '
                             'train_samples should be equal to total number '
                             'of samples in the extracted dataset')
    parser.add_argument('--train_samples', type=int, default=225,
                        help='Number of training samples per class to use')
    parser.add_argument('--epochs', type=int, default=10000,
                        help='Number of training epochs')
    parser.add_argument('--epochs_8bit', type=int, default=10000,
                        help='Number of fake 8-bit training epochs')
    parser.add_argument('--batch_size', type=int, default=64,
                        help='Size of training batch')
    parser.add_argument('--kernels', type=int, default=200,
                        help='Number of kernels in first convolution layer')
    parser.add_argument('--kernel_size', type=int, default=5,
                        help='Size of a kernel in first convolution layer')
    parser.add_argument('--verify', type=bool, default=False,
                        help='Whether to verify final TFLite model against test dataset')
    parser.add_argument('--continue_from', type=ContinueFrom, choices=list(ContinueFrom), default=ContinueFrom.prepare_sets,
                        help='Step to continue from')
    return parser.parse_args()


def main(args):
  quantizer = Quantizer(args.artifacts_path, args.model_name, args.kernels, args.kernel_size)

  if args.continue_from == ContinueFrom.prepare_sets:
    training_set, validation_set, test_set = quantizer.prepare_sets(args.dataset_path, args.gt_path, args.train_samples, args.val_samples, args.balanced)
  else:
    training_set, validation_set, test_set = quantizer.load_sets()

  if args.continue_from == ContinueFrom.prepare_sets:
    quantizer.train_float(args.epochs, args.batch_size, False)
  elif args.continue_from == ContinueFrom.train_float:
    quantizer.train_float(args.epochs, args.batch_size, True)

  if args.continue_from == ContinueFrom.prepare_sets or args.continue_from == ContinueFrom.train_float:
    quantizer.train_fake_8bit(args.epochs_8bit, args.batch_size, False)
  elif args.continue_from == ContinueFrom.train_fake_8bit:
    quantizer.train_fake_8bit(args.epochs_8bit, args.batch_size, True)

  if (args.continue_from == ContinueFrom.prepare_sets or
      args.continue_from == ContinueFrom.train_float or
      args.continue_from == ContinueFrom.train_fake_8bit or
      args.continue_from == ContinueFrom.freeze):
    quantizer.freeze()

  if (args.continue_from == ContinueFrom.prepare_sets or
      args.continue_from == ContinueFrom.train_float or
      args.continue_from == ContinueFrom.train_fake_8bit or
      args.continue_from == ContinueFrom.freeze or
      args.continue_from == ContinueFrom.convert):
    quantizer.convert()

  if args.continue_from == ContinueFrom.verify or args.verify:
    run_interpreter(args.artifacts_path, args.model_name, test_set)


if __name__ == "__main__":
  main(parse_args())
